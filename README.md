# [Reciprocate my portfolio]

Step 1: Download template of your choice, I choose to use https://startbootstrap.com/theme/stylish-portfolio

![picture](Githubimgs/bootstrap.png)

Step 2: Edit Index and and css page to you liking

![picture](Githubimgs/indexcss.png)

Step 3: Add video and use <muted, loop and autoplay> 

![picture](Githubimgs/muted.png)

Step 4: To add confetti for your subscription page I used https://github.com/mathusummut/confetti.js/

![picture](Githubimgs/confetti.png)

Choose MIT License since all code is open source and free.

[Assignment1Handin.html](https://bitbucket.org/Ericqua/assignment1bitbucket)
